﻿using QuizApp.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace QuizApp.Views
{
    public sealed partial class QuestionTextBox : UserControl
    {
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(QuestionTextBox), new PropertyMetadata(null));

        public bool IsCorrect
        {
            get { return (bool)GetValue(IsCorrectProperty); }
            set { SetValue(IsCorrectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsCorrect.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCorrectProperty =
            DependencyProperty.Register("IsCorrect", typeof(bool), typeof(QuestionTextBox), new PropertyMetadata(false));


        public bool IsAnswer
        {
            get { return (bool)GetValue(IsAnswerProperty); }
            set { SetValue(IsAnswerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsAnswer.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsAnswerProperty =
            DependencyProperty.Register("IsAnswer", typeof(bool), typeof(QuestionTextBox), new PropertyMetadata(true));

        public ICommand IsCorrectChangedCommand
        {
            get { return (ICommand)GetValue(IsCorrectChangedCommandProperty); }
            set { SetValue(IsCorrectChangedCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsCorrectChangedCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCorrectChangedCommandProperty =
            DependencyProperty.Register("IsCorrectChangedCommand", typeof(ICommand), typeof(QuestionTextBox), new PropertyMetadata(null));

        public string AnswerCode { get; set; }

        public QuestionTextBox()
        {
            this.InitializeComponent();
            grid.DataContext = this;
        }
    }
}
