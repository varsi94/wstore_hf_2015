﻿using QuizApp.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace QuizApp.Views
{
    public sealed partial class QuestionUserControl : UserControl
    {
        public Question Question
        {
            get { return (Question)GetValue(QuestionProperty); }
            set { SetValue(QuestionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Question.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty QuestionProperty =
            DependencyProperty.Register("Question", typeof(Question), typeof(QuestionUserControl), new PropertyMetadata(null));

        public ICommand WrongAnswerCommand
        {
            get { return (ICommand)GetValue(WrongAnswerCommandProperty); }
            set { SetValue(WrongAnswerCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WrongAnswerCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WrongAnswerCommandProperty =
            DependencyProperty.Register("WrongAnswerCommand", typeof(ICommand), typeof(QuestionUserControl), new PropertyMetadata(null));

        public ICommand GoodAnswerCommand
        {
            get { return (ICommand)GetValue(GoodAnswerCommandProperty); }
            set { SetValue(GoodAnswerCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GoodAnswerCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GoodAnswerCommandProperty =
            DependencyProperty.Register("GoodAnswerCommand", typeof(ICommand), typeof(QuestionUserControl), new PropertyMetadata(null));

        public QuestionUserControl()
        {
            this.InitializeComponent();
        }
    }
}
