﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApp.Data
{
    public static class ListExtension
    {
        public static void Shuffle<T>(this List<T> list)
        {
            Random r = new Random();
            int n = list.Count - 1;
            while (n > 0)
            {
                int k = r.Next(n);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
                n--;
            }
        }
    }
}
