﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QuizApp.RestClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace QuizApp.Data
{
    public static class GameUtils
    {
        public async static Task<Question> GetRandomQuestion(int difficulty)
        {
            using (var client = new ParseClient())
            {
                string s = await client.GetRandomQuestion(difficulty);
                using (var reader = new StringReader(s))
                {
                    var serializer = new JsonSerializer();
                    var obj = (JObject)serializer.Deserialize(new JsonTextReader(reader));
                    var result = obj.GetValue("result").ToObject(typeof(Question));
                    return (Question)result;
                }
            }
        }

        public async static Task<bool> CreateQuestion(Question q, string language)
        {
            using (var client = new ParseClient())
            {
                return await client.CreateQuestion(q, language);
            }
        }
    }
}
