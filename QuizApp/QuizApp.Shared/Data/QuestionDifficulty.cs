﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace QuizApp.Data
{
    public class QuestionDifficulty : ObservableObject
    {
        public int Difficulty { get; set; }
        public string Cash { get; set; }

        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set { Set(ref isSelected, value); }
        }


        public QuestionDifficulty(int difficulty, string cash)
        {
            Difficulty = difficulty;
            Cash = cash;
            IsSelected = false;
        }

        public static ObservableCollection<QuestionDifficulty> GetDifficulties()
        {
            var res = new ObservableCollection<QuestionDifficulty>();
            for (int i = 1; i <= 15; i++)
            {
                res.Add(new QuestionDifficulty(i, ResourceHelper.GetString("Award" + i.ToString())));
            }
            return res;
        }
    }
}
