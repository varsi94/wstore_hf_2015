﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QuizApp.RestClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace QuizApp.Data
{
    [ParseObjectName("Question")]
    [JsonObject]
    [DataContract]
    public class Question : ParseObjectBase<Question>
    {
        [DataMember]
        [JsonProperty("correct")]
        public string CorrectAnswer { get; set; }
        [DataMember]
        [JsonProperty("question")]
        public string QuestionString { get; set; }
        [DataMember]
        [JsonProperty("difficulty")]
        public int Difficulty { get; set; }
        [DataMember]
        public string A { get; set; }
        [DataMember]
        public string B { get; set; }
        [DataMember]
        public string C { get; set; }
        [DataMember]
        public string D { get; set; }

        public class Answer
        {
            public string Title { get; set; }
            public bool IsCorrect { get; set; }

            public Answer(string title, bool isCorrect)
            {
                Title = title;
                IsCorrect = isCorrect;
            }
        }

        public List<Answer> GetShuffledAnswers()
        {
            var result = new List<Answer>()
            {
                new Answer(A, CorrectAnswer == "A"),
                new Answer(B, CorrectAnswer == "B"),
                new Answer(C, CorrectAnswer == "C"),
                new Answer(D, CorrectAnswer == "D")
            };
            result.Shuffle();
            return result;
        }

        public bool IsValid()
        {
            return !String.IsNullOrEmpty(QuestionString) && !String.IsNullOrEmpty(CorrectAnswer) && !String.IsNullOrEmpty(A) && !String.IsNullOrEmpty(B)
                && !String.IsNullOrEmpty(C) && !String.IsNullOrEmpty(D);
        }
    }
}
