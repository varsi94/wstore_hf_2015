﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using System.Linq;

namespace QuizApp.Data
{
    [DataContract]
    public class Game
    {
        private static readonly string FileName = "saveGame.xml";
        [DataMember]
        private Dictionary<int, Question> questions;
        [DataMember]
        public int CurrentQuestionId { get; set; }
        [IgnoreDataMember]
        public Question CurrentQuestion
        {
            get
            {
                return questions[CurrentQuestionId];
            }
        }
        [DataMember]
        public bool HasPhoneHelp { get; set; }
        [DataMember]
        public bool HasAudienceHelp { get; set; }
        [DataMember]
        public bool HasHalvingHelp { get; set; }

        protected Game()
        {
            questions = new Dictionary<int, Question>();
            CurrentQuestionId = 1;

            HasHalvingHelp = true;
            HasAudienceHelp = true;
            HasPhoneHelp = true;
        }

        public async static Task<Game> CreateGame()
        {
            Game g = new Game();
            await g.RetrieveQuestions();
            return g;
        }

        private async Task RetrieveQuestions()
        {
            for (int i = 1; i <= 15; i++)
            {
                Question q = await GameUtils.GetRandomQuestion(i);
                questions.Add(i, q);
            }
        }

        public Question GetNextQuestion()
        {
            if (CurrentQuestionId == 15)
            {
                return null;
            }
            else
            {
                return questions[++CurrentQuestionId];
            }
        }

        public void Halving()
        {
            List<string> answers = new List<string>();
            for (int i = 0; i < 4; i++)
            {
                string s = Convert.ToChar('A' + i).ToString();
                if (s != CurrentQuestion.CorrectAnswer)
                {
                    answers.Add(s);
                }
            }

            for (int i = 0; i < 2; i++)
            {
                Random r = new Random();
                int x = r.Next(0, answers.Count);
                DisableAnswer(answers[x]);
                answers.RemoveAt(x);
            }
        }

        private void DisableAnswer(string code)
        {
            switch (code)
            {
                case "A":
                    CurrentQuestion.A = "";
                    break;
                case "B":
                    CurrentQuestion.B = "";
                    break;
                case "C":
                    CurrentQuestion.C = "";
                    break;
                case "D":
                    CurrentQuestion.D = "";
                    break;
                default:
                    break;
            }
        }

        public async Task<bool> Save()
        {
            StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(FileName, CreationCollisionOption.ReplaceExisting);
            try
            {
                var dcs = new DataContractSerializer(typeof(Game));
                var stringBuilder = new StringBuilder();
                using (var xml = XmlWriter.Create(stringBuilder, new XmlWriterSettings()
                {
                    Async = true
                }))
                {
                    dcs.WriteObject(xml, this);
                    await xml.FlushAsync();
                    XmlDocument document = new XmlDocument();
                    string content = stringBuilder.ToString();
                    document.LoadXml(content);
                    await document.SaveToFileAsync(file);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async static Task<Game> Load()
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(FileName);
                var dcs = new DataContractSerializer(typeof(Game));
                string content = null;
                using (var fs = await file.OpenStreamForReadAsync())
                using (StreamReader reader = new StreamReader(fs))
                {
                    content = await reader.ReadToEndAsync();
                    Game g = (Game)dcs.ReadObject(XmlReader.Create(new StringReader(content)));
                    await file.DeleteAsync();
                    return g;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string PhoneHelp(Dictionary<string, string> answers)
        {
            int goodAnswerP = 80 - (CurrentQuestionId - 1) * 3;
            Random r = new Random();
            int P = r.Next(0, 101);
            if (P <= goodAnswerP)
            {
                //jó válasz
                return CurrentQuestion.CorrectAnswer;
            }
            else
            {
                //rossz válasz
                List<string> badAnswers = new List<string>();
                foreach (var item in answers)
                {
                    if (item.Key != CurrentQuestion.CorrectAnswer || item.Value == "")
                    {
                        badAnswers.Add(item.Value);
                    }
                }

                int x = r.Next(0, answers.Count);
                return answers.Keys.ToList()[x];
            }
        }

        public Dictionary<string, int> AudienceHelp()
        {
            var result = new Dictionary<string, int>();
            Random r = new Random();

            int ossz = 100;
            string maxAnswer = null;

            for (int i = 0; i < 4; i++)
            {
                string s = Convert.ToChar('A' + i).ToString();
                if (i == 3)
                {
                    result.Add(s, ossz);
                }
                else
                {
                    result.Add(s, r.Next(0, ossz));
                }
                ossz -= result[s];
                if (maxAnswer == null || result[s] > result[maxAnswer])
                {
                    maxAnswer = s;
                }
            }

            int jo = 85 - CurrentQuestionId * 2;
            int percentage = r.Next(0, 101);
            string goodAnswer = CurrentQuestion.CorrectAnswer;
            if (percentage <= jo)                                /* korrigálunk az eredményen, ha szükséges */
            {
                /* jó választ ad a közönség */
                if (goodAnswer != maxAnswer)
                {
                    int d;
                    d=result[maxAnswer];
                    result[maxAnswer] = result[goodAnswer];
                    result[goodAnswer] = d;
                }
            }
            else
            {
                /* rossz választ ad a közönség */
                if (maxAnswer == goodAnswer)
                {
                    int d;
                    string cserealap;
                    do
                        cserealap = Convert.ToChar(r.Next(0, 4) + 'A').ToString();
                    while (cserealap != maxAnswer);
                    d=result[maxAnswer];
                    result[maxAnswer] = result[cserealap];
                    result[cserealap] = d;
                    maxAnswer = cserealap;
                }
            }

            return result;
        }
    }
}
