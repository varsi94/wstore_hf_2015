﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.ApplicationModel.Resources;

namespace QuizApp.Data
{
    public class ResourceHelper
    {
        public static string GetString(string key)
        {
            return ResourceLoader.GetForCurrentView().GetString(key);
        }
    }
}
