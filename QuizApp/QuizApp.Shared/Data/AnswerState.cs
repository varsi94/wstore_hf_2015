﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApp.Data
{
    public enum AnswerState
    {
        Default, Marked, Correct, Wrong
    }
}
