﻿using Newtonsoft.Json;
using QuizApp.RestClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.Serialization;

namespace QuizApp.Data
{
    [DataContract]
    public abstract class ParseObjectBase<T> where T : ParseObjectBase<T>
    {
        [DataMember]
        [JsonProperty("objectId")]
        public string ObjectId { get; set; }
        [JsonProperty("createdAt")]
        [DataMember]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("updatedAt")]
        [DataMember]
        public DateTime UpdatedAt { get; set; }

        public static async Task<T> GetFromParse(string objectId)
        {
            using (ParseClient client = new ParseClient())
            {
                var response = await client.GetObjectResponse(GetParseClassName(), objectId);
                var serializer = new JsonSerializer();
                using (var stringReader = new StringReader(response))
                {
                    var reader = new JsonTextReader(stringReader);
                    var resultObj = (T)serializer.Deserialize(reader, typeof(T));
                    return resultObj;
                }
            }
        }

        private static string GetParseClassName() 
        {
            var type = typeof(T);
            var attr = (ParseObjectName)type.GetTypeInfo().GetCustomAttribute(typeof(ParseObjectName));
            return attr.ObjectName;
        }
    }
}
