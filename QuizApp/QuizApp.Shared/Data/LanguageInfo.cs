﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.ApplicationModel.Resources;

namespace QuizApp.Data
{
    public class LanguageInfo
    {
        public string DisplayName { get; set; }
        public string LanguageCode { get; set; }

        protected LanguageInfo()
        {

        }

        public static List<LanguageInfo> GetLanguageInfo()
        {
            List<LanguageInfo> result = new List<LanguageInfo>();
            result.Add(new LanguageInfo()
            {
                DisplayName = ResourceLoader.GetForCurrentView().GetString("Hungarian"),
                LanguageCode = "hu"
            });

            result.Add(new LanguageInfo()
            {
                DisplayName = ResourceLoader.GetForCurrentView().GetString("English"),
                LanguageCode = "en"

            });
            return result;
        }
    }
}
