﻿using System;

[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
sealed class ParseObjectName : Attribute
{
    readonly string objectName;

    public ParseObjectName(string positionalString)
    {
        this.objectName = positionalString;
    }

    public string ObjectName
    {
        get { return objectName; }
    }
}