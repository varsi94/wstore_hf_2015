﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.UI.Popups;

namespace QuizApp.Dialogs
{
    public class YesNoDialogHelper
    {
        public static async Task<bool> CreateYesNoDialog(string label)
        {
            MessageDialog dialog = new MessageDialog(label);
            IUICommand yesCommand = new UICommand(ResourceLoader.GetForCurrentView().GetString("Yes"));
            IUICommand noCommand = new UICommand(ResourceLoader.GetForCurrentView().GetString("No"));

            dialog.Commands.Add(yesCommand);
            dialog.Commands.Add(noCommand);

            IUICommand result = await dialog.ShowAsync();
            return result == yesCommand;
        }
    }
}
