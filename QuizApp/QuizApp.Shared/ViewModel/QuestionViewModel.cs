﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using QuizApp.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace QuizApp.ViewModel
{
    public class QuestionViewModel : ViewModelBase
    {
        private Question question;
        public Question Question
        {
            get { return question; }
            set
            {
                Set(ref question, value);

                for (int i = 0; i < 4; i++)
                {
                    string key = Convert.ToChar('A' + i).ToString();
                    AnswerStates[key] = AnswerState.Default;
                }
                RaisePropertyChanged(() => AnswerStates);
                UpdateAnswers();
            }
        }

        public ICommand MarkQuestionCommand { get; set; }
        public GameViewModel GameVM { get; set; }
        public Dictionary<string, AnswerState> AnswerStates { get; set; }
        public Dictionary<string, string> Answers { get; set; }

        public QuestionViewModel()
        {
            InitializeCommands();
            AnswerStates = new Dictionary<string, AnswerState>();
            Answers = new Dictionary<string, string>();
            AnswerStates.Add("A", AnswerState.Default);
            AnswerStates.Add("B", AnswerState.Default);
            AnswerStates.Add("C", AnswerState.Default);
            AnswerStates.Add("D", AnswerState.Default);
        }

        private void InitializeCommands()
        {
            MarkQuestionCommand = new RelayCommand<string>((s) => ExecuteMarkQuestion(s));
        }

        public void UpdateAnswers()
        {
            Answers.Clear();
            Answers.Add("A", Question.A);
            Answers.Add("B", Question.B);
            Answers.Add("C", Question.C);
            Answers.Add("D", Question.D);
            RaisePropertyChanged(() => Answers);
        }

        private async void ExecuteMarkQuestion(string code)
        {
            if (String.IsNullOrEmpty(Answers[code]))
                return;
            AnswerStates[code] = AnswerState.Marked;
            RaisePropertyChanged(() => AnswerStates);
            await Task.Delay(TimeSpan.FromSeconds(3));
            if (code == Question.CorrectAnswer)
            {
                AnswerStates[code] = AnswerState.Correct;
                RaisePropertyChanged(() => AnswerStates);
                await Task.Delay(TimeSpan.FromSeconds(3));
                await GameVM.CorrectAnswer();
            }
            else
            {
                AnswerStates[code] = AnswerState.Wrong;
                AnswerStates[Question.CorrectAnswer] = AnswerState.Correct;
                RaisePropertyChanged(() => AnswerStates);
                await Task.Delay(TimeSpan.FromSeconds(3));
                await GameVM.GameLost();
            }
        }
    }
}
