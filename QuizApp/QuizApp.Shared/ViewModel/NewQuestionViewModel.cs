﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using QuizApp.Data;
using QuizApp.Dialogs;
using QuizApp.Views;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Windows.Input;
using Windows.ApplicationModel.Resources;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;
using Windows.UI.Popups;

namespace QuizApp.ViewModel
{
    public class NewQuestionViewModel : ViewModelBase
    {
        public ICommand GoBackCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand CorrectChangedCommand { get; set; }
        public Question Question { get; set; }
        public LanguageInfo Language { get; set; }
        public List<LanguageInfo> Languages { get; set; }

        public NewQuestionViewModel()
        {
            InitializeCommands();

            Question = new Question();
            Question.CorrectAnswer = "A";
            Question.Difficulty = 1;

            Languages = LanguageInfo.GetLanguageInfo();
            string culture = GetCulture();
            foreach (var item in Languages)
            {
                if (item.LanguageCode == culture)
                {
                    Language = item;
                    RaisePropertyChanged(() => Language);
                    break;
                }
            }
        }

        private string GetCulture()
        {
            CultureInfo info = CultureInfo.CurrentCulture;
            if (info.Name == "hu-HU" || info.Name == "hu")
            {
                return "hu";
            }
            else
            {
                return "en";
            }
        }

        private void InitializeCommands()
        {
            GoBackCommand = new RelayCommand(() => ExecuteGoBack());
            SaveCommand = new RelayCommand(() => ExecuteSave());
            CorrectChangedCommand = new RelayCommand<string>((s) => ExecuteCorrectChanged(s));
        }

        private void ExecuteCorrectChanged(string s)
        {
            Question.CorrectAnswer = s;
            RaisePropertyChanged(() => Question);
        }

        private async void ExecuteSave()
        {
            if (Question.IsValid())
            {
                if (await GameUtils.CreateQuestion(Question, Language.LanguageCode))
                {
                    ShowToast(ResourceHelper.GetString("UploadQuestionSuccess"));
                    App.GoBack();
                }
                else
                {
                    ShowToast(ResourceHelper.GetString("UploadQuestionFailed"));
                }
            }
            else
            {
                MessageDialog dialog = new MessageDialog(ResourceHelper.GetString("QuestionNotValid"));
                dialog.Commands.Add(new UICommand(ResourceHelper.GetString("OK")));
                await dialog.ShowAsync();
            }
        }

        private void ShowToast(string message)
        {
            ToastTemplateType templateType = ToastTemplateType.ToastText02;
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(templateType);

            XmlNodeList toastTextElements = toastXml.GetElementsByTagName("text");
            toastTextElements[0].AppendChild(toastXml.CreateTextNode(message));

            ToastNotification toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private async void ExecuteGoBack()
        {
            if (await YesNoDialogHelper.CreateYesNoDialog(ResourceHelper.GetString("ConfirmGoBack")))
            {
                if (App.CanGoBack())
                {
                    App.GoBack();
                }
                else
                {
                    App.Navigate(typeof(MainPage), null);
                }
            }
        }
    }
}
