﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using QuizApp.Data;
using QuizApp.Dialogs;
using QuizApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls.Primitives;

namespace QuizApp.ViewModel
{
    public class GameViewModel : ViewModelBase
    {
        public QuestionViewModel QuestionVM { get; set; }
        public ObservableCollection<QuestionDifficulty> Difficulties { get; set; }
        
        private Game currentGame;
        public Game CurrentGame
        {
            get { return currentGame; }
            set
            {
                currentGame = value;
                QuestionVM = new QuestionViewModel()
                {
                    Question = value.CurrentQuestion,
                    GameVM = this
                };
                RaisePropertyChanged(() => QuestionVM);
                IsLoaded = true;
                Difficulties[CurrentGame.CurrentQuestionId - 1].IsSelected = true;
                InitializeCommands();
            }
        }

        private ICommand goBackCommand;
        public ICommand GoBackCommand
        {
            get { return goBackCommand; }
            set { Set(ref goBackCommand, value); }
        }

        private RelayCommand phoneHelpCommand;
        public RelayCommand PhoneHelpCommand
        {
            get { return phoneHelpCommand; }
            set { Set(ref phoneHelpCommand, value); }
        }

        private RelayCommand halvingHelpCommand;
        public RelayCommand HalvingHelpCommand
        {
            get { return halvingHelpCommand; }
            set { Set(ref halvingHelpCommand, value); }
        }

        private RelayCommand audienceHelpCommand;
        public RelayCommand AudienceHelpCommand
        {
            get { return audienceHelpCommand; }
            set { Set(ref audienceHelpCommand, value); ; }
        }

        private ICommand stopCommand;
        public ICommand StopCommand
        {
            get { return stopCommand; }
            set { Set(ref stopCommand, value); }
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { Set(ref saveCommand, value); }
        }

        private bool isLoaded;
        public bool IsLoaded
        {
            get { return isLoaded; }
            set { Set(ref isLoaded, value); }
        }

        private Dictionary<string,int> audienceHelpData;
        public Dictionary<string,int> AudienceHelpData
        {
            get { return audienceHelpData; }
            set { Set(ref audienceHelpData, value); }
        }


        public GameViewModel()
        {
            IsLoaded = false;
            Difficulties = QuestionDifficulty.GetDifficulties();
        }

        private void InitializeCommands()
        {
            GoBackCommand = new RelayCommand(() => ExecuteGoBack());
            PhoneHelpCommand = new RelayCommand(() => ExecutePhoneHelp(), () => CurrentGame.HasPhoneHelp);
            HalvingHelpCommand = new RelayCommand(() => ExecuteHalvingHelp(), () => CurrentGame.HasHalvingHelp);
            AudienceHelpCommand = new RelayCommand(() => ExecuteAudienceHelp(), () => CurrentGame.HasAudienceHelp);
            StopCommand = new RelayCommand(() => ExecuteStop());
            SaveCommand = new RelayCommand(() => ExecuteSave());
        }

        private async void ExecuteSave()
        {
            if (await CurrentGame.Save())
            {
                GoBack();
            }
            else
            {
                await new MessageDialog(ResourceHelper.GetString("FileSaveError")).ShowAsync();
            }
        }

        private async void ExecuteStop()
        {
            string prize = (CurrentGame.CurrentQuestionId == 1) ? ResourceHelper.GetString("NoAward") : Difficulties[CurrentGame.CurrentQuestionId - 2].Cash;
            string content = String.Format(ResourceHelper.GetString("GameStopped"), prize);
            MessageDialog dialog = new MessageDialog(content, ResourceHelper.GetString("GameStoppedTitle"));
            await dialog.ShowAsync();
            GoBack();
        }

        private void ExecuteAudienceHelp()
        {
            AudienceHelpData = CurrentGame.AudienceHelp();

            CurrentGame.HasAudienceHelp = false;
            AudienceHelpCommand.RaiseCanExecuteChanged();
        }

        private void ExecuteHalvingHelp()
        {
            CurrentGame.Halving();

            CurrentGame.HasHalvingHelp = false;
            HalvingHelpCommand.RaiseCanExecuteChanged();
            QuestionVM.UpdateAnswers();
        }

        private async Task ExecutePhoneHelp()
        {
            string answer = CurrentGame.PhoneHelp(QuestionVM.Answers);
            string content = String.Format(ResourceHelper.GetString("PhoneHelp"), answer);
            string title = ResourceHelper.GetString("PhoneHelpTitle");
            await new MessageDialog(content, title).ShowAsync();

            CurrentGame.HasPhoneHelp = false;
            PhoneHelpCommand.RaiseCanExecuteChanged();
        }

        private void GoBack()
        {
            if (App.CanGoBack())
            {
                App.GoBack();
            }
            else
            {
                App.Navigate(typeof(MainPage), null);
            }
        }

        private async void ExecuteGoBack()
        {
            if (await YesNoDialogHelper.CreateYesNoDialog(ResourceHelper.GetString("ConfirmGoBack")))
            {
                GoBack();
            }
        }

        public async Task CreateGame()
        {
            CurrentGame = await Game.CreateGame();
        }

        public async Task CorrectAnswer()
        {
            QuestionVM.Question = CurrentGame.GetNextQuestion();
            for (int i = 0; i < 15; i++)
            {
                Difficulties[i].IsSelected = (i == CurrentGame.CurrentQuestionId - 1);
            }

            if (CurrentGame.CurrentQuestion == null)
            {
                //főnyeremény
                string content = String.Format(ResourceHelper.GetString("GameWon"), Difficulties[14].Cash);
                string title = ResourceHelper.GetString("GameWonTitle");
                await new MessageDialog(content, title).ShowAsync();
            }
        }

        public async Task GameLost()
        {
            string prize;
            if (CurrentGame.CurrentQuestionId > 10)
            {
                prize = Difficulties[9].Cash;
            }
            else if (CurrentGame.CurrentQuestionId > 5)
            {
                prize = Difficulties[4].Cash;
            }
            else
            {
                prize = ResourceHelper.GetString("NoAward");
            }
            string content = String.Format(ResourceHelper.GetString("GameLost"), prize);
            MessageDialog dialog = new MessageDialog(content, ResourceHelper.GetString("GameLostTitle"));
            await dialog.ShowAsync();
            GoBack();
        }
    }
}
