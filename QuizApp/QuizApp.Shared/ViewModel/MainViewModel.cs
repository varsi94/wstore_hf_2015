﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using QuizApp.Data;
using QuizApp.Views;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.UI.Popups;

namespace QuizApp.ViewModel
{
    public partial class MainViewModel : ViewModelBase
    {
        public ICommand StartNewGameCommand { get; set; }
        public ICommand LoadSavedGameCommand { get; set; }
        public ICommand UploadQuestionCommand { get; set; }
        public ICommand ShowHighscoresCommand { get; set; }

        public MainViewModel()
        {
            InitalizeCommands();
        }

        private void InitalizeCommands()
        {
            StartNewGameCommand = new RelayCommand(() => ExecuteStartNewGame());
            LoadSavedGameCommand = new RelayCommand(() => ExecuteLoadSavedGame());
            UploadQuestionCommand = new RelayCommand(() => ExecuteUploadQuestion());
            ShowHighscoresCommand = new RelayCommand(() => ExecuteShowHighScores());
        }

        private void ExecuteShowHighScores()
        {
            
        }

        private void ExecuteUploadQuestion()
        {
            App.Navigate(typeof(NewQuestionPage), null);
        }

        private async void ExecuteLoadSavedGame()
        {
            Game g = await Game.Load();
            if (g == null)
            {
                await new MessageDialog(ResourceHelper.GetString("FileLoadFailed")).ShowAsync();
            }
            else
            {
                App.Navigate(typeof(GamePage), g);
            }
        }

        private void ExecuteStartNewGame()
        {
            App.Navigate(typeof(GamePage), null);
        }
    }
}
