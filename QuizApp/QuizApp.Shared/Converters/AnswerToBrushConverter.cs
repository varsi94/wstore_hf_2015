﻿using QuizApp.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace QuizApp.Converters
{
    public class AnswerToBrushConverter : IValueConverter
    {
        public Brush DefaultBrush { get; set; }
        public Brush MarkedBrush { get; set; }
        public Brush CorrectBrush { get; set; }
        public Brush WrongBrush { get; set; }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            AnswerState answerState = (AnswerState)value;
            switch (answerState)
            {
                case AnswerState.Default:
                    return DefaultBrush;
                case AnswerState.Marked:
                    return MarkedBrush;
                case AnswerState.Correct:
                    return CorrectBrush;
                case AnswerState.Wrong:
                    return WrongBrush;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
