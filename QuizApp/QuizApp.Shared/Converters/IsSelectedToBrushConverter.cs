﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace QuizApp.Converters
{
    public class IsSelectedToBrushConverter : IValueConverter
    {
        public Brush Default { get; set; }
        public Brush Selected { get; set; }
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((bool)value) ? Selected : Default;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
