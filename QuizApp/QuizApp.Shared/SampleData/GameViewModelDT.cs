﻿using QuizApp.Data;
using QuizApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace QuizApp.SampleData
{
    public class GameViewModelDT
    {
        public QuestionViewModel QuestionVM
        {
            get
            {
                return new QuestionViewModel()
                {
                    Question = new Question()
                    {
                        A = "A válasz",
                        B = "B válasz",
                        C = "C válasz",
                        D = "D válasz",
                        QuestionString = "Kérdés",
                        CorrectAnswer = "A"
                    },
                    AnswerStates = new Dictionary<string, AnswerState>()
                    {
                        { "A", AnswerState.Correct},
                        {"B", AnswerState.Default},
                        {"C", AnswerState.Marked},
                        {"D", AnswerState.Wrong}
                    }
                };
            }
        }

        public ObservableCollection<QuestionDifficulty> Difficulties
        {
            get
            {
                return QuestionDifficulty.GetDifficulties();
            }
        }
    }
}
