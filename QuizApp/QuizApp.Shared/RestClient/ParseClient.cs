﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QuizApp.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace QuizApp.RestClient
{
    /// <summary>
    /// Class for getting response from Parse server.
    /// </summary>
    public class ParseClient : IDisposable
    {
        private HttpClient client;
        public ParseClient()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-Parse-Application-Id", ApplicationInfo.XParseApplicationId);
            client.DefaultRequestHeaders.Add("X-Parse-REST-API-KEY", ApplicationInfo.XParseRESTAPIKey);
        }

        /// <summary>
        /// Get response for retrieving an object from server
        /// </summary>
        /// <param name="className">className in Parse</param>
        /// <param name="objectId">objectId field</param>
        /// <returns>Json string representing the object</returns>
        public async Task<string> GetObjectResponse(string className, string objectId)
        {
            var url = new Uri(String.Format("https://api.parse.com/1/classes/{0}/{1}", className, objectId));
            var obj = await client.GetAsync(url);
            return await obj.Content.ReadAsStringAsync();
        }

        private string GetCulture()
        {
            CultureInfo info = CultureInfo.CurrentCulture;
            if (info.Name == "hu-HU" || info.Name == "hu")
            {
                return "hu";
            }
            else
            {
                return "en";
            }
        }

        /// <summary>
        /// Calling getRandomQuestion cloud function.
        /// </summary>
        /// <param name="difficulty">The difficulty of the question.</param>
        /// <returns>Json string representing the response.</returns>
        public async Task<string> GetRandomQuestion(int difficulty)
        {
            var url = new Uri("https://api.parse.com/1/functions/getRandomQuestion");
            string language = GetCulture();
            var param = new
            {
                difficulty = difficulty,
                language = language
            };
            using(var writer = new StringWriter())
	        {
                var textWriter = new JsonTextWriter(writer);
                new JsonSerializer().Serialize(textWriter, param);
                var obj = await client.PostAsync(url, new StringContent(writer.ToString()));
                return await obj.Content.ReadAsStringAsync();
	        }
        }

        /// <summary>
        /// Insert question into table. It adds automatically the language parameter by current culture info.
        /// </summary>
        /// <param name="question">Question object to upload</param>
        /// <returns>the operation was successful</returns>
        public async Task<bool> CreateQuestion(Question question, string language)
        {
            var url = new Uri("https://api.parse.com/1/classes/Question");
            using (var writer = new StringWriter())
            {
                var textWriter = new JsonTextWriter(writer);
                var serializer = new JsonSerializer();
                var jsonObj = JObject.FromObject(question);
                jsonObj.Add("language", language);
                new JsonSerializer().Serialize(textWriter, jsonObj);
                var content = new StringContent(writer.ToString());
                content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                var obj = await client.PostAsync(url, content);
                return obj.IsSuccessStatusCode;
            }
        }

        public void Dispose()
        {
            client.Dispose();
        }
    }
}
